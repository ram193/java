import java.util.Scanner;
import java.util.*;

public class Array {
    void printDigit(int[] allowedDigits, int totalDigits) {
        ArrayList<String> arr=new ArrayList<>();
        ArrayList<String> arr1=new ArrayList<>();

        switch(totalDigits)
        {
            case 2:
            {
                for(int i=0;i<allowedDigits.length;i++)
                {
                    for(int j=0;j<allowedDigits.length;j++)
                    {
                        if(i!=j)
                        {
                            arr.add(String.format("%d%d",(i+1),(1+j)));
                            arr1.add(String.format("%d%d",(i+1),(1+j)));
                        }
                        else{
                            arr1.add(String.format("%d%d",(i+1),(1+j)));
                        }
                    }
                }
                break;
            }
            case 3:
            {
                for(int i=0;i<allowedDigits.length;i++)
                {
                    for(int j=0;j<allowedDigits.length;j++)
                    {
                        for(int k=0;k<allowedDigits.length;k++)
                        {
                            if(i!=j && j!=k && k!=i)
                            {
                                arr.add(String.format("%d%d%d",(i+1),(1+j),(k+1)));
                                arr1.add(String.format("%d%d%d",(i+1),(1+j),(k+1)));
                            }
                            else{
                                arr1.add(String.format("%d%d%d",(i+1),(1+j),(k+1)));
                            }
                        }
                        
                    }
                }
                break;
            }
            default:
                System.out.println("wrong choice");
                return;

        }
        System.out.println("With digit repetition:");
        for(String i:arr)
        {
            System.out.println(i);
        }
        System.out.println("without digit repetition");
        for(String i:arr1)
        {
            System.out.println(i);
        }
    }

    public static void main(String[] args) {
        Array a=new Array();
        Scanner myObj = new Scanner(System.in);
        System.out.print("Yor want no. Array");
        int arrNo = myObj.nextInt();
        int[] arr = new int[arrNo];
        for (int i = 0; i < arrNo; i++) {
            System.out.print("enter no. Array element:" + (i + 1) + "  ");
            arr[i] = myObj.nextInt();
        }
        System.out.print("Yor want total digit:(2-3) ");
        int digit = myObj.nextInt();
        a.printDigit(arr,digit);
    }
}
