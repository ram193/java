package javaProject;

public class GenericDoubleNode<T>{


		class Node{
			T name;
			Node before;
			Node next;
			Node(T name)
			{
				this.name=name;
				this.before=null;
				this.next=null;
			}
		}
		Node head=null;
		Node tail=null;
		Node addItem(T name)
		{
			Node new_node=new Node(name);
			if(this.head == null)
			{
				this.head=this.tail=new_node;
			}
			else {
				this.tail.next=new_node;
				new_node.before=this.tail;
				this.tail=new_node;
			}
			return new_node;
		}
		
		void delete(int index)
		{
			 int intIndex=0;
		        if(index==0)
		        {
		        	
		            this.head=this.head.next;
		            this.head.before=null;
		        }
		        else{
		            Node nextNode=this.head;
		            
		            while(intIndex<index-1)
		            {
		                nextNode=nextNode.next;
		                intIndex++;
		            }
		            if(this.tail==nextNode.next)
		            {
		                this.tail=nextNode;
		                nextNode.next=null;
		            }
		            else{
		            int move_value=2;
		            Node curNode=nextNode;
		            while(move_value>0)
		            {
		                curNode=curNode.next;
		                move_value--;
		            }
		                nextNode.next=curNode;
		                curNode.before=nextNode;
		                
		            }
		        }
		       
		}
		void unique()
		{
			Node n=this.head;
			int index=0;
			while(n!=null)
			{
				Node n1=n.next;
				while(n1!=null)
				{
					if(n.name==n1.name)
					{
						this.delete(index);
					}
					n1=n1.next;
				}
				n=n.next;
				index++;
				
			}
		}
		void insert(int index,T name)
		{
			 int intIndex=0;
			 Node new_node=new Node(name);
		        if(index==0)
		        {
		        	new_node.next=this.head;
		        	this.head.before=new_node;
		        	this.head=new_node;
		        }
		        else{
		            Node nextNode=this.head;
		            
		            while(intIndex<index-1)
		            {
		                nextNode=nextNode.next;
		                intIndex++;
		            }
		            Node curNode=nextNode.next;
//		            System.out.println(this.tail.name);
		            if(this.tail==nextNode.next)
		            {
		            	curNode.next=new_node;
		            	new_node.before=curNode;
		                this.tail=new_node;
	
		            }
		            else{
//		            	 System.out.println("jjs"+nextNode.name);
//				            System.out.println("jdjs"+curNode.name);
		            	nextNode.next=new_node;
		            	new_node.before=nextNode;
		            	new_node.next=curNode;
		            	curNode.before=new_node;
		                
		            }
		        }
		}
		GenericDoubleNode copy()
		{
			GenericDoubleNode<T> list=new GenericDoubleNode();
			Node n=this.head;
			while(n!=null)
			{
				list.addItem(n.name);
				n=n.next;
			}
			return list;
		}
		void print() {
			Node n=this.head;
			while(n!=null)
			{
				System.out.println(n.name);
				n=n.next;
			}
		}
		void backprint() {
			Node n=this.tail;
			while(n!=null)
			{
				System.out.println(n.name);
				n=n.before;
			}
		}
		public static void main(String[] args) {
		// TODO Auto-generated method stub
		GenericDoubleNode<String> list=new GenericDoubleNode();
		list.addItem("A");
		list.addItem("A");
		list.addItem("B");
		list.addItem("C");
		list.addItem("D");
		list.addItem("E");
		list.addItem("E");
		list.addItem("E");
		list.addItem("E");
//		list.insert(4,"Z");
//		list.delete(3);
//		list.backprint();
		list.unique();
		GenericDoubleNode<String> copylist= list.copy();
		copylist.print();
		
	}

}
