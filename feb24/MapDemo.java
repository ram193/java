package javaProject;

import java.util.*;
import java.util.Map.Entry;

public class MapDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 Map<Integer,String> map=new HashMap<Integer,String>();
		 map.put(1,"jai");
		 map.put(2,"kumar");
		 map.put(3,"rani");
		 map.put(4,"ramesh");
		 Set<Entry<Integer,String>> set=map.entrySet();
		 Iterator<Entry<Integer,String>> itr=set.iterator();
		 while(itr.hasNext()) {
			 Map.Entry<Integer,String> entry=(Map.Entry<Integer,String>) itr.next();
			 System.out.println(entry.getKey()+" "+entry.getValue());
		 }
		 for(Map.Entry m:map.entrySet() )
		 {
			 System.out.println(m.getKey()+" "+m.getValue());
		 }
	}

}
