	package javaProject;

public class GenericLinkedLIst <T>{
	class Node{
		T value;
		Node next;

		Node(T value){
			this.value=value;
			this.next=null;
		}
	}
	Node head;
	Node tail;
	
	GenericLinkedLIst(){
		this.head=null;
		this.tail=null;
	}
	Node addItem(T value) {
		Node node=new Node(value);
		if(this.head==null)
		{	
			this.head=this.tail=node;
			
		}else
		{
			this.tail.next=node;
			this.tail=node;
		}
		return node;
	}
	void print()
	{
		Node n=this.head;
		 while(n != null) {
	            System.out.println(n.value);
	            n = n.next;
	}}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		GenericLinkedLIst<Integer> list=new GenericLinkedLIst();
		list.addItem(1);
		list.addItem(2);
		list.addItem(3);
		list.print();
	}

}
