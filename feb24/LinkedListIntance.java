package javaProject;

import java.util.*;

class Address{
	String name;
	String address;
	String city;
	String code;
	Address(String name,String address,String city,String code)
	{
		this.name=name;
		this.address=address;
		this.city=city;
		this.code=code;
	}
	public String toString() {
		return name+" "+address+" "+city+" "+code;
	}
}
public class LinkedListIntance {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LinkedList<Address> a=new LinkedList<Address>();
		a.add(new Address("jai","132  keelapalayam street","Tenkasi","628393"));
		a.add(new Address("kumar","13  kulathai street","kumbakomam","628423"));
		a.add(new Address("nandhu","134 palayam street","Tenkasi","63393"));
		a.add(new Address("laxmi","132  keela street","mathalamparai","88393"));
		for(Address i:a)
		{
			System.out.println(i);
			System.out.println();
		}
	}

}
