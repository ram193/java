import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.File;

public class XML {
    private final String xmlPath = "//home/zoho/Downloads/students.xml";
    DocumentBuilderFactory xmlFactory;

    private String getFirstNodeText(NodeList nodes) {
        return nodes.item(0).getTextContent();
    }

    void print() {
        xmlFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder db = xmlFactory.newDocumentBuilder();
            Document doc = db.parse(new File(xmlPath));
            Element rootElement = doc.getDocumentElement();
            NodeList students = rootElement.getElementsByTagName("student");
            for(int i = 0; i < students.getLength(); i++) {
                Element studentNode = (Element) students.item(i);
                String id = getFirstNodeText(studentNode.getElementsByTagName("id"));
                String firstName = getFirstNodeText(studentNode.getElementsByTagName("firstname"));
                String lastName = getFirstNodeText(studentNode.getElementsByTagName("lastname"));

                System.out.println(id + ": " + firstName + " " + lastName);
            }
        } catch (Exception e) {

        }
    }

    public static void main(String args[]) {
        XML xml = new XML();
        xml.print();
    }
}