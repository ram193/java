package javaProject;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class MapFileRead {
	private final File file;
	MapFileRead(String path)
	{
		file=new File(path);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MapFileRead n= new MapFileRead("//home/zoho/Downloads/callme.txt");
		Map<String,String> map=new HashMap<String,String>();
		try(Scanner scanner=new Scanner(n.file))
		{
			while(scanner.hasNextLine())	
			{
				String fileData=scanner.nextLine();
				String[] str=fileData.split(",");
				map.put(str[0],str[1]);
			}
		}
		catch(FileNotFoundException e)
		{
			System.out.println("File not found err");
			e.printStackTrace();	 
		}
		catch(Exception ex)
		{
			System.out.println("errr");
			ex.printStackTrace();
			
		}
		 for(Map.Entry m:map.entrySet() )
		 {
			 System.out.println(m.getKey()+" "+m.getValue());
		 }
	}

}
