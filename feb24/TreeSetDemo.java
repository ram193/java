package javaProject;

import java.util.*;

	 class MyComp implements Comparator<String>{
	
		public int compare(String s1,String s2) {
			int i=s1.indexOf(" ");
			int i1=s2.indexOf(" ");
			String s3=s1.substring(i);
			String s4=s2.substring(i1);
			
			return s4.compareTo(s3);
		}
	}


public class TreeSetDemo {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TreeSet<String> ts= new TreeSet<String>(new MyComp());
		ts.add("A B");
		ts.add("Z Q");
		ts.add("P R");
		ts.add("M C");
		for(String i:ts) {
			System.out.println(i);
		}
		

	}

}
