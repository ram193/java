package javaProject;
import java.io.File;  
import java.io.FileNotFoundException; 
import java.util.Scanner;

public class FileRead {
	private final File file;
	FileRead(String path)
	{
		file=new File(path);
	}
	
	void printInfo()
	{
		if(file.exists())
		{
			System.out.println("File name:"+file.getName());
			System.out.println("Full path:"+file.getAbsolutePath());
			System.out.println("Writeable"+file.canWrite());
			System.out.println("Readable:"+file.canRead());
			System.out.println("File sie in bytes:"+file.length());
		}
		else {
			System.out.println("The file does not exits.");
		}
	}
	void printContents() {
		try(Scanner scanner=new Scanner(file))
		{
			while(scanner.hasNextLine())
			{
				String fileData=scanner.nextLine();
				System.out.println(fileData);
			}
		}
		catch(FileNotFoundException e)
		{
			System.out.println("File not found err");
			e.printStackTrace();	 
		}
		catch(Exception ex)
		{
			System.out.println("errr");
			ex.printStackTrace();
			
		}
	}
	
	public static void main(String[] args) {
			
		FileRead n=new FileRead("//home/zoho/Downloads/students.xml");
		n.printInfo();
		n.printContents();
	}

}
