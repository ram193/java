class FibonacciSum{    
	public static void main(String[] args) {
		long i =1;
		long s = 2;
		long sum = 0;
		long total = 0;
		while(i<=4000000){
			total = i;
			i = s;
			s += total;
			if(total%2 == 0){
				sum += total;
			}
		}
		System.out.println(sum);
	}
}