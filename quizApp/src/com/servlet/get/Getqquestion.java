package com.servlet.get;

import java.util.*;
import java.sql.*;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilderFactory;  
import javax.xml.parsers.DocumentBuilder;  
import org.w3c.dom.Document;  
import org.w3c.dom.NodeList;  
import org.w3c.dom.Node;  
import org.w3c.dom.Element;  
import java.io.File;  



public class Getqquestion{
	 Map<Integer,CreateQuiz> map=new HashMap<Integer,CreateQuiz>();
     public Map<Integer,CreateQuiz> read()
    {
    try   
    {  
  
    File file = new File("XMLFile.xml");  
   
    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();  
  
    DocumentBuilder db = dbf.newDocumentBuilder();  
    Document doc = db.parse(file);  
    doc.getDocumentElement().normalize();  
    NodeList nodeList = doc.getElementsByTagName("question");  
   
  
    for (int itr = 0; itr < nodeList.getLength(); itr++)   
    {  
    Node node = nodeList.item(itr);  
    CreateQuiz n=new CreateQuiz();
    if (node.getNodeType() == Node.ELEMENT_NODE)   
    {  
    Element eElement = (Element) node;  
    String data= eElement.getElementsByTagName("quest").item(0).getTextContent();
    String data1=eElement.getElementsByTagName("ans").item(0).getTextContent();
    n.setAns(data1);
    n.setQuestion(data);
    map.put(itr,n);
    }
    }
 
    }   
    catch (Exception e)   
    {  
    e.printStackTrace();  
    System.out.print(e);
    }  
    return map;
    }
    
             

    
}  
